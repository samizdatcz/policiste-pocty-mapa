topo = ig.data.data
geojson = topojson.feature topo, topo.objects."data"

container = ig.containers.base
mapElement = document.createElement \div
  ..id = \map
container.appendChild mapElement
map = L.map do
  * mapElement
  * minZoom: 7,
    maxZoom: 13,
    zoom: 7
    center: [49.78, 15.5]
    maxBounds: [[48.3,11.6], [51.3,19.1]]

baseLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png"
  * attribution: 'mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'

labelLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_l1/{z}/{x}/{y}.png"

L.Icon.Default.imagePath = "https://samizdat.cz/tools/leaflet/images"

baseLayer.addTo map
labelLayer.addTo map

console.log geojson
max = d3.max geojson.features.map (.properties.found)
breaks = ig.utils.divideToParts [334, max], 5
  ..pop!
colorScale = d3.scale.threshold!
  ..domain breaks
  ..range <[#762a83 #fe9929 #ec7014 #cc4c02 #993404]>

detailBox = d3.select container .append \div
  ..attr \id \detail

name = detailBox.append \h2
content = detailBox.append \p

highlight = ({properties}) ->
  name.html properties.OOP_NAZEV
  content.html "V roce 2015 zde řešili <b>#{ig.utils.formatNumber properties.found}</b> případů. Z nich #{ig.utils.formatNumber properties.solved} vyřešili, objasněnost tedy je #{ig.utils.formatNumber properties.solved / properties.found * 100} %"

downlight = ->
  name.html "Většina okrsků řeší méně než 1 případ denně"
  content.html "Fialově jsou označeny okrsky s méně než 334 řešenými trestnými činy. Po najetí myši nad okrsek se zde zobrazí počet jím řešených trestných činů a jejich objasněnost."
downlight!
geoJsonLayer = L.geoJson do
  * geojson
  * style: (feature, layer) ->
      color = colorScale feature.properties.found
      stroke: no
      fillColor: color
      fillOpacity: 0.8
      smoothFactor: 0.1
    onEachFeature: (feature, layer) ->
      layer.on \mouseover -> highlight feature
      layer.on \click -> highlight feature
      layer.on \mouseout downlight
geoJsonLayer.addTo map
